import { ImageSlider } from './ImageSlider/ImageSlider';
//
import img1 from './imgs/1.jpg';
import img2 from './imgs/2.jpg';
import img3 from './imgs/3.jpg';
import img4 from './imgs/4.jpg';
import img5 from './imgs/5.jpg';
import img6 from './imgs/6.jpg';

const IMAGES = [
  { url: img1, alt: 'Lorem Text 1' },
  { url: img2, alt: 'Lorem Text 2' },
  { url: img3, alt: 'Lorem Text 3' },
  { url: img4, alt: 'Lorem Text 4' },
  { url: img5, alt: 'Lorem Text 5' },
  { url: img6, alt: 'Lorem Text 6' },
];

export const App = () => {
  return (
    <div
      style={{
        maxWidth: '800px',
        width: '100%',
        aspectRatio: '10 / 4',
        margin: '0 auto',
      }}
    >
      <ImageSlider images={IMAGES} />
      <a href="/">next content</a>
    </div>
  );
};
