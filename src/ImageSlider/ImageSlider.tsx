import { useState } from 'react';
import { ChevronLeft, ChevronRight, Circle, CircleDot } from 'lucide-react';
//
import './image-slider.css';
//

type ImagesSliderProps = {
  images: { url: string; alt: string }[];
};

export const ImageSlider = ({ images }: ImagesSliderProps) => {
  const [imageIndex, setImageIndex] = useState(0);

  const showPrevImg = () => {
    setImageIndex((index) => {
      if (index === images.length - 1) return 0;
      return index + 1;
    });
  };

  const showNextImg = () => {
    setImageIndex((index) => {
      if (index === 0) return images.length - 1;
      return index - 1;
    });
  };

  return (
    <section
      aria-label="Image Slider"
      style={{ width: '100%', height: '100%', position: 'relative' }}
    >
      <a href="#after-image-slider-controls" className="skip-link">
        Skip image slider controls
      </a>
      <div
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
          overflow: 'hidden',
        }}
      >
        {images.map(({ url, alt }, index) => (
          <img
            key={url}
            className="img-slider-img"
            src={url}
            aria-hidden={imageIndex !== index}
            style={{
              translate: `${-100 * imageIndex}%`,
            }}
            alt={alt}
          />
        ))}
      </div>
      <button
        className="img-slider-btn"
        style={{ left: 0 }}
        onClick={showNextImg}
        aria-label="View Next Image"
      >
        <ChevronLeft aria-hidden />
      </button>
      <button
        className="img-slider-btn"
        style={{ right: 0 }}
        onClick={showPrevImg}
        aria-label="View Previous Image"
      >
        <ChevronRight aria-hidden />
      </button>
      <div
        style={{
          position: 'absolute',
          bottom: '0.5rem',
          left: '50%',
          translate: '-50%',
          display: 'flex',
        }}
      >
        {images.map((_, index) => (
          <button
            key={index}
            className="img-slider-dot-btn"
            onClick={() => setImageIndex(index)}
            aria-label={`View ${index + 1} Image`}
          >
            {index === imageIndex ? (
              <CircleDot aria-hidden />
            ) : (
              <Circle aria-hidden />
            )}
          </button>
        ))}
      </div>
      <div id="after-image-slider-controls" />
    </section>
  );
};
